package es.apitechu.hackaton.repositories;

import es.apitechu.hackaton.models.AccountModel;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AccountRepository extends MongoRepository<AccountModel, String> {

}
