package es.apitechu.hackaton.repositories;


import es.apitechu.hackaton.models.CardModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends MongoRepository<CardModel, String> {
}
