package es.apitechu.hackaton.controllers;



import es.apitechu.hackaton.models.CardModel;
import es.apitechu.hackaton.services.CardService;
import es.apitechu.hackaton.services.CardServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v0/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST})

public class CardController {


    @Autowired
    CardService cardService;

    //CREATE

    @PostMapping("/cards")
    public ResponseEntity<CardServiceResponse> save(@RequestBody CardModel cardModel) {
        System.out.println("------ save card ------");

        CardServiceResponse result =this.cardService.save(cardModel);
        return new ResponseEntity<>(result,result.getHttpsStatus());
    }

    // READ ALL
    @GetMapping("/cards")
    public ResponseEntity<CardServiceResponse>   findAll() {
        System.out.println("------ findAll cards ------");
        CardServiceResponse result = new CardServiceResponse();
        result= this.cardService.findAll();
        return new ResponseEntity<>(result, result.getHttpsStatus());

    }

    // READ BY ID
    @GetMapping("/cards/{id}")
    public ResponseEntity<CardServiceResponse> getByDNI(@PathVariable String id) {

        System.out.println("------ get by DNI ------");
        CardServiceResponse result = new CardServiceResponse();
        result = cardService.findAllbyDNI(id);

        return new ResponseEntity<>(result, result.getHttpsStatus());

    }

    //DELETE

    @DeleteMapping("/cards/{id}")
    public boolean deleteCard(@PathVariable String id) {
        if (cardService.existsById(id)) {
            CardModel card = cardService.findAllbyDNI(id).getCards().get(1);
            cardService.isDeleted(card);
            return true;
        } else
            return false;
    }
}
