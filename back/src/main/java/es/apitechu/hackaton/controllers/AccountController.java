package es.apitechu.hackaton.controllers;


import es.apitechu.hackaton.models.AccountModel;
import es.apitechu.hackaton.services.AccountService;
import es.apitechu.hackaton.services.AccountServiceResponse;
import es.apitechu.hackaton.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@RequestMapping("/hackaton/v0/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})

public class AccountController {


    @Autowired
    AccountService accountService;

    @Autowired
    UserService userServices;

    //CREATE

    @PostMapping("/accounts")
    public ResponseEntity<AccountServiceResponse> save(@RequestBody AccountModel accountModel) {
        AccountServiceResponse result = new AccountServiceResponse();
        System.out.println("------ save Account ------");
        boolean existUser = userServices.existsById(accountModel.getDni());
        if (existUser) {
            Random rand = new Random();
            String id = String.format("%04d", rand.nextInt(10000));
            String account = "ES31-0182-0000-0000-000-" + id;
            accountModel.setNumber(account);
            accountModel.setBalance(0);
            result = this.accountService.save(accountModel);
            return new ResponseEntity<>(result, result.getHttpsStatus());

        } else {

            result.setHttpsStatus(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(result, result.getHttpsStatus());
        }

    }

    // READ ALL
    @GetMapping("/accounts")
    public ResponseEntity<AccountServiceResponse> findAll() {
        System.out.println("------ findAll Accounts ------");
        AccountServiceResponse result = new AccountServiceResponse();
        result = this.accountService.findAll();
        return new ResponseEntity<>(result, result.getHttpsStatus());

    }

    // READ BY ID
    @GetMapping("/accounts/{id}")
    public ResponseEntity<AccountServiceResponse> getByDNI(@PathVariable String id) {

        System.out.println("------ get by DNI ------");
        AccountServiceResponse result = new AccountServiceResponse();
        result = accountService.findAllbyDNI(id);

        return new ResponseEntity<>(result, result.getHttpsStatus());

    }

    //DELETE

    @DeleteMapping("/accounts/{id}")
    public boolean deleteAccount(@PathVariable String id) {
        if (accountService.existsById(id)) {
            AccountModel account = accountService.findAllbyDNI(id).getAccounts().get(1);
            accountService.isDeleted(account);
            return true;
        } else
            return false;
    }
}
