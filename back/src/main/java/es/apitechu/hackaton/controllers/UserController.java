package es.apitechu.hackaton.controllers;

import es.apitechu.hackaton.models.LoginModel;
import es.apitechu.hackaton.models.UserModel;
import es.apitechu.hackaton.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.undertow.UndertowWebServer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v0/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST})

public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser (@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("El ID del usuario que se va a crear es: "+ user.getId());
        System.out.println("El DNI del usuario que se va a crear es: "+ user.getDni());
        System.out.println("El NOMBRE del usuario que se va a crear es: "+ user.getNombre());
        System.out.println("El APELLIDO1 del usuario que se va a crear es: "+ user.getApellido1());
        System.out.println("El APELLIDO2 del usuario que se va a crear es: "+ user.getApellido2());
        System.out.println("El PASSWORD del usuario que se va a crear es: "+ user.getPassword());
        System.out.println("El LINKIMAGE del usuario que se va a crear es: "+ user.getLinkimage());
        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);

    }

    @PostMapping("/login")
    public ResponseEntity<String> login (@RequestBody LoginModel login) {
        System.out.println("login");
        System.out.println("Login del usuario con DNI: : "+ login.getDni());
        System.out.println("Login del usuario con Password:: "+ login.getPassword());
        boolean resultadologin = false;

        if (login.getDni()!=null && login.getPassword()!=null) {
            resultadologin = this.userService.login(login.getDni(), login.getPassword());
        }

        if (resultadologin) {
            return new ResponseEntity<>("Usuario: "+ login.getDni() + " autorizado.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Autenticación del usuario: "+ login.getDni() + " no autorizada.", HttpStatus.FORBIDDEN);
        }
    }
}
