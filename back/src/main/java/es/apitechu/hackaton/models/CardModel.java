package es.apitechu.hackaton.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cards")
public class CardModel {

    String dni;
    String number;
    String description;


    public CardModel() {
    }

    public CardModel(String dni, String number, String description) {
        this.dni = dni;
        this.number = number;
        this.description = description;

    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
