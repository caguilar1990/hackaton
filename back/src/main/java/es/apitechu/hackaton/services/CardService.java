package es.apitechu.hackaton.services;

import es.apitechu.hackaton.models.CardModel;
import es.apitechu.hackaton.repositories.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CardService {

    @Autowired
    CardRepository cardRepository;

    public CardServiceResponse save(CardModel cardModel) {
        CardServiceResponse result = new CardServiceResponse();
        result.setHttpsStatus(HttpStatus.CREATED);
        result.setCard(cardModel);
        result.setMsg("Tarjeta creada correctamente");
        cardRepository.save(cardModel);
        return result;
    }

    public CardServiceResponse findAll() {
        CardServiceResponse result = new CardServiceResponse();
        if (cardRepository.findAll().isEmpty()) {
            result.setHttpsStatus(HttpStatus.NO_CONTENT);
            result.setMsg("No hay Datos");

        } else {
            result.setCards(cardRepository.findAll());
            result.setHttpsStatus(HttpStatus.OK);
            result.setMsg("Listado Correcto");
        }
        return result;

    }

    public CardServiceResponse findAllbyDNI(String id) {
        CardServiceResponse result = new CardServiceResponse();
        List<CardModel> listDNI = new ArrayList<>();
        List<CardModel> allList = new ArrayList<>();

        allList = cardRepository.findAll();
        System.out.println(allList.size());
        System.out.println("ID buscado: " + id);
        for (int i = 0; i < allList.size(); i++) {

            System.out.println(allList.get(i).getDni());

            if (allList.get(i).getDni().equals(id)) {
                System.out.println("entro en if");
                listDNI.add(allList.get(i));

            }

        }
        if (listDNI.isEmpty()) {
            result.setMsg("No existen datos para ese cliente");
            result.setHttpsStatus(HttpStatus.NO_CONTENT);
        } else {
            result.setCards(listDNI);
            result.setHttpsStatus(HttpStatus.OK);
            result.setMsg("Listado Correcto");
        }


        return result;
    }


    public boolean isDeleted(CardModel card) {
        try {
            cardRepository.delete(card);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean existsById(String id) {
        return cardRepository.existsById(id);
    }

    public Optional<CardModel> findById(String id) {
        return cardRepository.findById(id);
    }
}
