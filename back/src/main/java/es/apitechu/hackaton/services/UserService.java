package es.apitechu.hackaton.services;

import es.apitechu.hackaton.models.UserModel;
import es.apitechu.hackaton.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserModel add (UserModel user) {
        System.out.println("Estoy en add");
        return this.userRepository.save(user);
    }

    public boolean login (String dni,String password){
        System.out.println("findById()");
        System.out.println("Obteniendo el usuario con el id " + dni);
        Optional<UserModel> result = this.userRepository.findById(dni);
        //UserModel result = this.userRepository.findById(dni);

        //UserModel aa = result;
        //UserModel pass = result;
        boolean validarlogin = false;
        if (result.isPresent() == true){
            String valdni = result.get().getDni();
            String valpassword = result.get().getPassword();
            if (dni.equals(valdni) && password.equals(valpassword)) {
                System.out.println("USUARIO AUTENTICADO");
                validarlogin = true;
            }
        } else {
            System.out.println("USUARIO NO AUTENTICADO");
            validarlogin = false;
        }
        return validarlogin;
    }
    public boolean existsById(String id) {
        return userRepository.existsById(id);
    }
}
