
package es.apitechu.hackaton.services;


import es.apitechu.hackaton.models.AccountModel;
import es.apitechu.hackaton.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;


    //CREATE
    public AccountServiceResponse save(AccountModel accountModel) {

        AccountServiceResponse result = new AccountServiceResponse();
        result.setHttpsStatus(HttpStatus.CREATED);
        result.setMsg("Cuenta creada correctamente");
        result.setAccount(accountModel);
        accountRepository.save(accountModel);
        return result;
    }

    // READ ALL
    public AccountServiceResponse findAll() {
        AccountServiceResponse result = new AccountServiceResponse();
        if (accountRepository.findAll().isEmpty()) {
            result.setHttpsStatus(HttpStatus.NO_CONTENT);
            result.setMsg("No hay Datos");

        } else {
            result.setAccounts(accountRepository.findAll());
            result.setHttpsStatus(HttpStatus.OK);
            result.setMsg("Listado Correcto");
        }
        return result;

    }

    // READ BY ID
    public Optional<AccountModel> findById(String id) {
        return accountRepository.findById(id);
    }

    public AccountServiceResponse findAllbyDNI(String id) {
        AccountServiceResponse result = new AccountServiceResponse();
        List<AccountModel> listDNI = new ArrayList<>();
        List<AccountModel> allList = new ArrayList<>();

        allList = accountRepository.findAll();
        System.out.println(allList.size());
        System.out.println("ID buscado: " + id);
        for (int i = 0; i < allList.size(); i++) {

            System.out.println(allList.get(i).getDni());

            if (allList.get(i).getDni().equals(id)) {
                System.out.println("entro en if");
                listDNI.add(allList.get(i));

            }

        }
        if (listDNI.isEmpty()) {
            result.setMsg("No existen datos para ese cliente");
            result.setHttpsStatus(HttpStatus.NO_CONTENT);
        } else {
            result.setAccounts(listDNI);
            result.setHttpsStatus(HttpStatus.OK);
            result.setMsg("Listado Correcto");
        }


        return result;


    }

    // DELETE
    public boolean isDeleted(AccountModel deleteAccount) {
        try {
            accountRepository.delete(deleteAccount);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean existsById(String id) {
        return accountRepository.existsById(id);
    }
}
