package es.apitechu.hackaton.services;

import es.apitechu.hackaton.models.AccountModel;
import es.apitechu.hackaton.models.CardModel;
import org.springframework.http.HttpStatus;

import java.util.List;

public class CardServiceResponse {

    private String msg;
    private List<CardModel> cards;
    private CardModel card;
    private HttpStatus httpsStatus;

    public CardServiceResponse() {
    }

    public CardServiceResponse(String msg, List<CardModel> cards, CardModel card, HttpStatus httpsStatus) {
        this.msg = msg;
        this.cards = cards;
        this.card = card;
        this.httpsStatus = httpsStatus;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<CardModel> getCards() {
        return cards;
    }

    public void setCards(List<CardModel> cards) {
        this.cards = cards;
    }

    public CardModel getCard() {
        return card;
    }

    public void setCard(CardModel card) {
        this.card = card;
    }

    public HttpStatus getHttpsStatus() {
        return httpsStatus;
    }

    public void setHttpsStatus(HttpStatus httpsStatus) {
        this.httpsStatus = httpsStatus;
    }
}
