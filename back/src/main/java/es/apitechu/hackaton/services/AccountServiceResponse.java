package es.apitechu.hackaton.services;

import es.apitechu.hackaton.models.AccountModel;
import org.springframework.http.HttpStatus;

import java.util.List;

public class AccountServiceResponse {
    private String msg;
    private List<AccountModel> accounts;
    private AccountModel account;
    private HttpStatus httpsStatus;

    public AccountServiceResponse() {
    }

    public AccountServiceResponse(String msg, List<AccountModel> accounts, AccountModel account, HttpStatus httpsStatus) {
        this.msg = msg;
        this.accounts = accounts;
        this.account = account;
        this.httpsStatus = httpsStatus;
    }


    public List<AccountModel> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public HttpStatus getHttpsStatus() {
        return httpsStatus;
    }

    public void setHttpsStatus(HttpStatus httpsStatus) {
        this.httpsStatus = httpsStatus;
    }
}
