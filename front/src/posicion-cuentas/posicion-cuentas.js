import { LitElement, html} from 'lit-element' ;

class PosicionCuentas extends LitElement{

    static get properties(){
        return {
            
            dni:{type:String},
            number:{type:String},
            description:{type:String},
            balance:{type:String},
            currency: {type:String}
    
        }
    }

    constructor(){
        super();

        
        
    }
    render(){
        return html`
           
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            
            <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">${this.number}</h5>
                <small>${this.currency}</small>
                </div>
                <p class="mb-1">${this.description}</p>
                <small>Saldo: ${this.balance}</small>
            </a>
            </div>
            
        `;
    }

    
}

customElements.define('posicion-cuentas', PosicionCuentas);