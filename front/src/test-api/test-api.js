import { LitElement, html} from 'lit-element' ;

class TestApi extends LitElement{

    static get properties(){
        return {
            palabra: {type: String}
        }
    }

    constructor(){
        super();

        this.palabra = "";
        this.getPalabra();
    }
    render(){
        return html`
            <h1> Vuestra API nos devuelve ${this.palabra}</h1>
        `;
    }

    getPalabra(){
        console.log("getPalabra");
        console.log("Obteniendo datos de la API");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);

                this.palabra = APIResponse.results;
                console.log(APIResponse);
            }
        };

        xhr.open("GET", "http://10.0.0.147:8080/");
        xhr.send();
        console.log("Fin de getPalabra");
    }
}

customElements.define('test-api', TestApi);