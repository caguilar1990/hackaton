import { LitElement, html} from 'lit-element' ;
import '../posicion-botones/posicion-botones.js';
import '../posicion-salir/posicion-salir.js';

class PosicionHeader extends LitElement{

    static get properties(){
        return {
            botones: {type: String}

        };
    }

    constructor(){
        super();

        this.botones = "I";
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="row text-center">
                <div class="col-10 text-center" style="height: 200px width: 80rem;"><h1>Nuestro Banco</h1></div>
                <posicion-botones @login-person="${this.loginPerson}" @new-person="${this.newPerson}" 
                    class="col-2" id="botonInicio">
                </posicion-botones>
                <posicion-salir id="botonSalir" @salir-person="${this.irInicio}">SALIR</posicion-salir>
            </div>
        `;
    }

    loginPerson(){
        console.log("loginPerson recogido en Header");

        this.dispatchEvent(new CustomEvent("login-person", {}));

    }

    newPerson(){
        console.log("newPerson recogido en Header");

        this.dispatchEvent(new CustomEvent("new-person", {}));

    }

    updated(changedProperties){
        console.log("updated");
        console.log(this.botones);

        if (changedProperties.has("botones")) {
            console.log("Puede salir");

            if (this.botones === "S"){
                console.log("Es S");
            
                this.shadowRoot.getElementById("botonInicio").classList.add("d-none");
                this.shadowRoot.getElementById("botonSalir").classList.remove("d-none");  
                
            } else {
                this.shadowRoot.getElementById("botonInicio").classList.remove("d-none");
                this.shadowRoot.getElementById("botonSalir").classList.add("d-none");
          
            }
           
        }
    }

    irInicio(){
        console.log("irInicio");
        this.botones = "I";

        this.dispatchEvent(new CustomEvent("foto-inicio", {}));

    }

}

customElements.define('posicion-header', PosicionHeader);