import { LitElement, html} from 'lit-element' ;
import '../posicion-cuentas/posicion-cuentas.js';
import '../posicion-altacuentaform/posicion-altacuentaform.js';


class PosicionDetalle extends LitElement{

    static get properties(){
        return {
            cuentasUser: {type: Array},
            user:{type:String},
            errormsg:{type:String}
        };
    }

    constructor(){
        super();
       
        this.cuentasUser=[];  
       this.user="";
       this.errormsg="OK";
    }

    render(){
        return html`
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <h2 class="text-center">Cuentas</h2>
        <div class="row">
            <div>
            <button type="button" @click="${this.nuevaCuenta}">Nueva cuenta</button>
            </div>
            
            <div id="cuentasList">
                
                    
                    ${this.cuentasUser.map(
                        cuenta => html`<posicion-cuentas 
                                        class="cols 8"
                                        dni="${cuenta.name}"
                                        balance="${cuenta.balance}"
                                        description="${cuenta.description}"
                                        currency="${cuenta.currency}"
                                        number="${cuenta.number}"
                                        ></posicion-cuentas>` 
                    )}
                    
                
            </div>
            
            <posicion-altacuentaform
                @posicion-form-OK="${this.refreshCuentas}" 
                class="d-none border rounded border-primary" 
                id="altaCuentaForm"
                user="${this.user}">
            </posicion-altacuentaform>
        
            
            <div class="d-none text-center" id="Error">${this.errormsg}</div>
        </div>
        `;
    }

    nuevaCuenta(){
        console.log("nuevaCuenta");
        this.shadowRoot.getElementById("cuentasList").classList.add("d-none");
        this.shadowRoot.getElementById("altaCuentaForm").classList.remove("d-none");
        this.shadowRoot.getElementById("Error").classList.add("d-none");
        
    }
    refreshCuentas(){
        this.shadowRoot.getElementById("cuentasList").classList.remove("d-none");
        this.shadowRoot.getElementById("altaCuentaForm").classList.add("d-none");
        this.getcuentasbyuser();
    }


   updated(changedProperties){
    if (changedProperties.has("user")) {
        console.log("Usuario logueado");
        console.log(this.user);
        this.getcuentasbyuser();
        
        
    }

   }

    getcuentasbyuser(){
        console.log("---getcuentasbyuser---");
        console.log("Obteniendo datos de la API...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log(xhr.status);
            if (xhr.status === 200){
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.cuentasUser = APIResponse.accounts;
                
                this.shadowRoot.getElementById("Error").classList.add("d-none");
                this.shadowRoot.getElementById("cuentasList").classList.remove("d-none");
                
                
            }
            if (xhr.status === 204){
                console.log("No tiene cuentas asignadas");
                this.errormsg="No tiene cuentas asociadas";
                this.shadowRoot.getElementById("Error").classList.remove("d-none");
                this.shadowRoot.getElementById("cuentasList").classList.add("d-none");
        

            }
        };
        console.log(this.user);
        console.log('http://10.0.0.154:8080/hackaton/v0/accounts/'+this.user)
        xhr.open("GET", 'http://10.0.0.154:8080/hackaton/v0/accounts/'+this.user);
        xhr.send();
        
        console.log("---Fin getcuentasbyuser---");
    }
}

   

customElements.define('posicion-detalle', PosicionDetalle);