import { LitElement, html} from 'lit-element' ;
import '../posicion-header/posicion-header.js';
import '../posicion-main/posicion-main.js';
import '../posicion-footer/posicion-footer.js';
//import '../posicion-sidebar/posicion-sidebar.js';

class PosicionApp extends LitElement{

    static get properties(){
        return {

        };
    }

    constructor(){
        super();

    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <posicion-header @login-person="${this.loginPerson}" @new-person="${this.newPerson}" id="cabecera" @foto-inicio="${this.fotoInicio}"></posicion-header>
            <div class="row">
                <posicion-main class="center-block" @entra-detalle="${this.entraPerson}" id="detalle"></posicion-main>
            <div>
            <posicion-footer></posicion-footer>
        `;
    }

    loginPerson(){
        console.log("loginPerson recogido en App");

        this.shadowRoot.querySelector("posicion-main").showPosicForm = "L";

    }

    newPerson(){
        console.log("newPerson recogido en App");

        this.shadowRoot.querySelector("posicion-main").showPosicForm = "R";

    }

    entraPerson	(){
        console.log("entraPerson");

        this.shadowRoot.getElementById("cabecera").botones = "S";

    }

    fotoInicio(){
        console.log("fotoInicio");

        this.shadowRoot.getElementById("detalle").botones = "I";
    }

}

customElements.define('posicion-app', PosicionApp);