import { LitElement, html } from 'lit-element' ;
import '../posicion-detalle/posicion-detalle.js';
import '../posicion-login/posicion-login.js';
import '../posicion-registro/posicion-registro.js';

class PosicionMain extends LitElement{

    static get properties(){
        return {
            usuario: {type: Object},
            showPosicForm: {type: String},
            botones: {type: String}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                dni: "12345678N",
                passwd: "pepe",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley"
                }
            }, {
                name: "Bruce Banner",
                dni: "11111111",
                passwd: "pepe",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner"
                }
            }, {
                name: "Eowyn",
                dni: "11111111F",
                passwd: "pepe",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn"
                }
            }
        ];

        this.formato = {
            dni: "61111111F",
        	password: "Password"
        }

        this.usuario = {};

        this.showPosicForm = "F";

        this.botones = "";
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <img src="./img/edificio.jpg" id="imagen" height="250" width="1050" class="d-none img-fluid rounded mx-auto d-block img-thumbnail"></img>    
            <div><posicion-detalle class="d-none rounded mx-auto d-block" id="personDet"></posicion-detalle></div>
            <div><posicion-login class="d-none" id="personLogin" @login-realizado="${this.getPerson}"></posicion-login></div>
            <div><posicion-registro class="d-none" id="personRegister" @alta-solicitada="${this.altaPersona}"></posicion-registro></div>
            <div class="row" id="ErrCuentas" class="d-none">Servicio no disponible temporalmente</div>
        `;
    }

    updated(changedProperties){
        console.log("Updated");

        if (changedProperties.has("showPosicForm")) {
            console.log("ha cambiado el valor de la propiedad showPosicForm en posicion-main " + this.showPosicForm);

            this.shadowRoot.getElementById("ErrCuentas").classList.add("d-none");

            if (this.showPosicForm === "L") {
                this.showPersonLogin();
            } else if (this.showPosicForm === "R") {
                this.showPersonRegister();
            } else if (this.showPosicForm === "F") {
                this.showFoto();
            } else {
                this.showPersonDetalle();
            }
        }

        if (changedProperties.has("botones")) {
            console.log("ha cambiado botones");
            this.showFoto();

        }

    }

    showPersonLogin(){
        console.log("showPersonLogin Muestra ventana de Login");

        this.shadowRoot.getElementById("personLogin").classList.remove("d-none");
        this.shadowRoot.getElementById("personDet").classList.add("d-none");
        this.shadowRoot.getElementById("personRegister").classList.add("d-none");
        this.shadowRoot.getElementById("imagen").classList.add("d-none");

    }

    showPersonRegister(){
        console.log("showPersonLogin Muestra ventana de Registro");

        this.shadowRoot.getElementById("personLogin").classList.add("d-none");
        this.shadowRoot.getElementById("personDet").classList.add("d-none");
        this.shadowRoot.getElementById("personRegister").classList.remove("d-none");
        this.shadowRoot.getElementById("imagen").classList.add("d-none");
    }
    
    showPersonDetalle(){
        console.log("showPersonLogin Muestra ventana de Detalle");

        this.shadowRoot.getElementById("personLogin").classList.add("d-none");
        this.shadowRoot.getElementById("personDet").classList.remove("d-none");
        this.shadowRoot.getElementById("personRegister").classList.add("d-none");
        this.shadowRoot.getElementById("imagen").classList.add("d-none");
    }
    
    showFoto(){
        console.log("showPersonLogin Muestra ventana de Imagen");

        this.shadowRoot.getElementById("personLogin").classList.add("d-none");
        this.shadowRoot.getElementById("personDet").classList.add("d-none");
        this.shadowRoot.getElementById("personRegister").classList.add("d-none");
        this.shadowRoot.getElementById("imagen").classList.remove("d-none");
    }

    showError(){
        this.shadowRoot.getElementById("ErrCuentas").classList.remove("d-none");
        this.shadowRoot.getElementById("personDet").classList.add("d-none");
        this.shadowRoot.getElementById("personLogin").classList.add("d-none");
        this.shadowRoot.getElementById("personRegister").classList.add("d-none");
        this.shadowRoot.getElementById("imagen").classList.add("d-none");
    }

    busPerson(e){
        console.log("buscaPerson");
        console.log("http://10.0.0.150:8080/hackaton/v0/users/" + e.detail.person.dni);

        /*this.people= this.people.map(
            person => person.dni === e.detail.person.dni ? this.llamarDetalle(e) : console.log("Persona no encontrada")
        );*/
        this.llamarDetalle(e);
    }

    llamarDetalle(e){
        console.log("llamarDetalle");
        console.log(e);

        this.shadowRoot.getElementById("personDet").user = e.detail.person.dni;
        this.dispatchEvent(new CustomEvent("entra-detalle", {}));
        this.showPersonDetalle();
    }

    getPerson(e){
        console.log("getPerson");
        console.log("Obteniendo datos de la persona");
        console.log(e.detail.person);

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                this.llamarDetalle(e)
            } else if (xhr.status === 403){
                this.showPersonRegister();
            }
        };

        xhr.open("POST", "http://10.0.0.154:8080/hackaton/v0/login/");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(e.detail.person));
        console.log("Fin de getPerson");
    }

    altaPersona(e){
        console.log("altaPersona");

        console.log("Alta de la persona");
        console.log(e.detail.person);

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 201){
                console.log("Alta correcta");
                this.llamarDetalle(e);
            } else {
                console.log("El error que devuelve es " + xhr.status);
            }
        };
        xhr.ontimeout = function (e) {
            console.log("Error TimeOut ");

          };

        xhr.open("POST", "http://10.0.0.154:8080/hackaton/v0/users/");

        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(e.detail.person));
        console.log("Fin de getPerson");

        
    }
}

customElements.define('posicion-main', PosicionMain);