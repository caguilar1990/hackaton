import { LitElement, html} from 'lit-element' ;


class PosicionRegistro extends LitElement{

    static get properties(){
        return {
            person: {type: Object}
        };
    }

    constructor(){
        super();

        this.person = {};

    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <div class="d-flex justify-content-center align-items-center container">
            <form class="col-4 border border-4 rounded border-primary">
                <div class="form-group" style="width:80%">
               
                    <label class="sr-only" for="ejemplo_password_2"></label>
                    <input type="text" @input="${this.nifRegistro}" class="form-control" placeholder="Introduce tu Nif" required />
            
                </div>
                <div class="form-group" style="width:80%">
                    <label class="sr-only" for="ejemplo_password_2"></label>
                    <input type="text" @input="${this.nombreRegistro}"  class="form-control" placeholder="Introduce Nombre" required />
                </div>
                <div class="form-group" style="width:80%">
                    <label class="sr-only" for="ejemplo_password_2"></label>
                    <input type="text" @input="${this.apellido1Registro}"  class="form-control" placeholder="Introduce Apellido1" required />
                 </div>  
                    <div class="form-group" style="width:80%">
                 <label class="sr-only" for="ejemplo_password_2"></label>
                    <input type="text" @input="${this.apellido2Registro}"  class="form-control" placeholder="Introduce Apellido2" required />
                </div>
                <div class="form-group" style="width:80%">
                    <label class="sr-only"></label>
                    <input type="text" @input="${this.passwdRegistro}"  class="form-control" placeholder="Introduce Password" required />
                </div>
                <div class="form-group" style="width:80%">
                    <label class="sr-only"></label>
                    <input type="text" @input="${this.imagenRegistro}"  disabled class="form-control" placeholder="persona.jpg" />
                    <label class="sr-only"></label>
                </div>
                <button @click="${this.altaPerson}" class="btn btn-success"><strong>Registrarse</strong></button>
            </form>
        </div>  
        `;
    }

    nifRegistro(e){
        console.log("nifRegistro");
        this.person.dni = e.target.value;
    }

    nombreRegistro(e){
        console.log("nombreRegistro");
        this.person.nombre = e.target.value;
    }

    apellido1Registro(e){
        console.log("apellido1Regisro");
        this.person.apellido1 = e.target.value;
    }

    apellido2Registro(e){
        console.log("apellido2Registro");
        this.person.apellido2 = e.target.value;
    }

    passwdRegistro(e){
        console.log("passwdRegistro");
        this.person.passwd = e.target.value;
    }

    imagenRegistro(){
        console.log("imagenRegisro");

        this.person.imagen = "./img/persona.jpg";
    }

    altaPerson(e){
        console.log("altaPerson");
        e.preventDefault();
        console.log(this.person);

        this.dispatchEvent(new CustomEvent("alta-solicitada", {
            detail: {
                person: {
                    id: this.person.dni,
                    dni: this.person.dni,
                    nombre: this.person.nombre,
                    apellido1: this.person.apellido1,
                    apellido2: this.person.apellido2,
                    password: this.person.passwd,
                    linkimage: "./img/persona.jpg"
                }
            }
        })
        );
    }
}

customElements.define('posicion-registro', PosicionRegistro);