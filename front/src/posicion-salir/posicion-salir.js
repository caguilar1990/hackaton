import { LitElement, html} from 'lit-element' ;


class PosicionSalir extends LitElement{

    static get properties(){
        return {

        };
    }

    constructor(){
        super();

    }

    render(){
        return html`
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
                <button @click="${this.salirUser}" type="button" class="btn btn-secondary col-2"><strong>Salir</strong></button>
                
        `;
    }

    salirUser(){
        console.log("salirUser");

        this.dispatchEvent(new CustomEvent("salir-person", {}));
    }


}

customElements.define('posicion-salir', PosicionSalir);