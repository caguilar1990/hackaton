import { LitElement, html} from 'lit-element' ;



class PosicionAltaCuentaForm extends LitElement{

    static get properties(){
        return{
            cuenta: {type: Object},
            user:{type:String},
            errormsg:{type:String}
        
        };
    }

   
    constructor(){
        super();
        this.resetFormData();
        this.user="";
        this.cuenta={

            "dni": "88888888888",
            "description":"Account 7",
            "currency":"EUR"
            }
        this.errormsg="OK";
      
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <div id="formularioAlta">
            <form>
                <div class="form-group">
                    <label>Divisa</label>
                    <input type="text" 
                    @input="${this.updateDivisa}" 
                    class="form-control" 
                    placeholder="Divisa"/>
                </div>
               
                <div class="form-group">
                    <label>Descripcion</label>
                    <textarea @input="${this.updateDescription}" type="text" class="form-control" placeholder="Descripción cuenta"></textarea>
                </div>
                
                <button @click="${this.goBack}" class="btn btn-primary">Cancelar</button>
                <button @click="${this.nuevaCuenta}" class="btn btn-success">Guardar</button>
            </form>
            
        </div>
        <div id="Error" class="d-none text-center">
            <div><h4>${this.errormsg}<h4></div>
            </br>
            </br>
            <div><button @click="${this.goBack}" class="btn btn-success">Ver cuentas</button></div>
        </div>
        `;

    }
    updateDivisa(e){
        console.log("updateDivisa posicion altacuenta");
        this.cuenta.currency=e.target.value;
    }
    updateDescription(e){
        console.log("updateDescription posicion altacuenta");
        this.cuenta.description=e.target.value;
    }
   

    
    goBack(e){
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("posicion-form-OK",{}));   
    }

    nuevaCuenta(e){
        console.log("nuevaCuenta");
        e.preventDefault();
        console.log("---nuevaCuenta---");
        console.log("Preparando alta de cuenta");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log(xhr.status);
            if (xhr.status === 201){
                console.log("Nueva cuenta dada de alta");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                
                this.errormsg="Nueva cuenta dada de alta";
                this.shadowRoot.getElementById("Error").classList.remove("d-none");
                this.shadowRoot.getElementById("formularioAlta").classList.add("d-none");
               
                
            }
            else{
                console.log("KO alta de cuenta");
                this.errormsg="Servicio temporalmente indisponible.Por favor contacte con su gestor";
                this.shadowRoot.getElementById("Error").classList.remove("d-none");
                this.shadowRoot.getElementById("formularioAlta").classList.add("d-none");
                
        

            }
        };

        this.cuenta.dni=this.user;
        console.log(this.cuenta);
        console.log('http://10.0.0.154:8080/hackaton/v0/accounts/')
        
        xhr.open("POST", 'http://10.0.0.154:8080/hackaton/v0/accounts/');
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(this.cuenta));
        
        console.log("---Fin AltaCuenta---");
    
        this.resetFormData();
        
    }

    resetFormData(){
        console.log("resetFormData");
        this.cuenta={};
        this.cuenta.currency="";
        this.cuenta.office="";
        this.cuenta.balance=0;
        this.cuenta.description="";
        this.cuenta.dni="";
    }
        
    
}


customElements.define('posicion-altacuentaform', PosicionAltaCuentaForm);