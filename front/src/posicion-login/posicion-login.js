import { LitElement, html} from 'lit-element' ;

class PosicionLogin extends LitElement{

    static get properties(){
        return {
            person: {type: Object}
        };
    }

    constructor(){
        super();

        this.person = {};
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="d-flex justify-content-center align-items-center container" >
                <form class="col-2 border border-4 rounded border-primary">
                    <div class="form-group">
                        <label>NIF</label>
                        <input type="text" @input="${this.nifTecleado}" class="form-control" placeholder="Nif" required/>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" @input="${this.passwdTecleada}"  class="form-control" placeholder="Password" required />
                    </div>
                    <button @click="${this.buscaPerson}" class="btn btn-success"><strong>Log in</strong></button>
                </form>
            </div>  
        `;
    }

    nifTecleado(e){
        console.log("nifTecleado");
        console.log("Ha introducido " + e.target.value);

        this.person.dni = e.target.value;

    }

    passwdTecleada(e){
        console.log("passwdTecleada");

        this.person.passwd = e.target.value;

    }

    buscaPerson(e){
        console.log("buscaPerson");
        e.preventDefault();

        console.log(this.person.dni +  " " + this.person.passwd);

        this.dispatchEvent(new CustomEvent("login-realizado", {
            detail: {
                person: {
                    dni: this.person.dni,
                    password: this.person.passwd
                }
            }
        })
        );
    }

}

customElements.define('posicion-login', PosicionLogin);